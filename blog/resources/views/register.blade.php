<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SanberBook</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: black;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
            <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcome" target="_self" method="post">
            @csrf
            <label for="firstname">First name:</label><br>
            <input type="text" id="firstname" name="firstname"><br><br>
            <label for="lastname">Last name:</label><br>
            <input type="text" id="lastname" name="lastname"><br><br>

            <label for="gender">Gender : </label><br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label><br><br>

            <label for="Language">Language :</label><br>
            <input type="checkbox" id="lang1" name="lang1" value="indonesia">
            <label for="lang2"> Indonesia</label><br>
            <input type="checkbox" id="lang2" name="lang2" value="Inggris">
            <label for="lang2"> Inggris</label><br>
            <input type="checkbox" id="lang3" name="lang3" value="Malaysia">
            <label for="lang2"> Malaysia</label><br><br>

            <label for="nationality">Nationality : </label><br>
            <select id="nationality" name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Arab">Arab</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Korea">South Korea</option>
            </select><br><br>

            <label for="bio">Bio :</label><br>
            <textarea name="message" rows="10" cols="30">
            </textarea><br>

            <input type="submit" value="Sign Up">
        </form> 
    </body>
</html>
